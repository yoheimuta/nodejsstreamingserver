#!/usr/bin/env node
var exec = require('child_process').exec;

var SERVER_ADDRESS  = '127.0.0.1';
var SERVER_PORT     = '8086';
var DIFF_TOP        = 63;

var WebSocketClient = require('websocket').client;

var client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('WebSocket client connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type !== 'utf8') {
            console.log('Lost the message');
            return;
        }
        console.log("Received: '" + message.utf8Data + "'");

        var params = message.utf8Data.split('/');
        if (params.length < 2) {
            console.log('Invalid message');
            return;
        }

        var x = Number(params[0]);
        var y = Number(params[1]) + DIFF_TOP;
        console.log("ClickAt: x:" + x + ", y:" + y);

        exec('cliclick -r c:' + x + ',' + y, function(err, stdout, stderr){
            /*
            if (err) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr)
                return;
            }

            console.log(err.code);
            //err.signal will be set to the signal that terminated the process
            console.log(err.signal);
            */
        });
    });
});

client.connect('ws://' + SERVER_ADDRESS + ':' + SERVER_PORT);
