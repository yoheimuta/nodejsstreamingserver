if( process.argv.length < 3 ) {
    console.log(
        'Usage: \n' +
        'node stream-server.js <secret> [<stream-port> <websocket-port>]'
    );
    process.exit();
}

var STREAM_SECRET      = process.argv[2],
    STREAM_PORT        = process.argv[3] || 8082,
    WEBSOCKET_PORT     = process.argv[4] || 8084,
    WEBSOCKET_SIM_PORT = process.argv[5] || 8086,
    WIDTH              = 320,
    HEIGHT             = 547,
    STREAM_MAGIC_BYTES = 'jsmp'; // Must be 4 bytes

var width  = WIDTH,
    height = HEIGHT;

// Websocket Server
var socketServer = new (require('ws').Server)({port: WEBSOCKET_PORT});
socketServer.on('connection', function(socket) {
    // Send magic bytes and video size to the newly connected socket
    // struct { char magic[4]; unsigned short width, height;}
    var streamHeader = new Buffer(8);
    streamHeader.write(STREAM_MAGIC_BYTES);
    streamHeader.writeUInt16BE(width, 4);
    streamHeader.writeUInt16BE(height, 6);
    socket.send(streamHeader, {binary:true});

    console.log( 'New WebSocket Connection ('+socketServer.clients.length+' total)' );

    socket.on('close', function(code, message){
        console.log( 'Disconnected WebSocket ('+socketServer.clients.length+' total)' );
    });

    socket.on('message', function(data, flags) {
        console.log('clickat=' + data);
        iosSocketServer.broadcast(data, {binary:false});
    });
});

socketServer.broadcast = function(data, opts) {
    for( var i in this.clients ) {
        if (this.clients[i].readyState == 1) {
            this.clients[i].send(data, opts);
        }
        else {
            console.log( 'Error: Client ('+i+') not connected.' );
        }
    }
};

// Websocket Server For iOS Simulator Control
var iosSocketServer = new (require('ws').Server)({port: WEBSOCKET_SIM_PORT});
iosSocketServer.on('connection', function(socket) {
    console.log( 'New WebSocket iphone-sim Connection ('+iosSocketServer.clients.length+' total)' );

    socket.on('close', function(code, message){
        console.log( 'Disconnected WebSocket iphone-sim ('+iosSocketServer.clients.length+' total)' );
    });
});

iosSocketServer.broadcast = function(data, opts) {
    for( var i in this.clients ) {
        if (this.clients[i].readyState == 1) {
            this.clients[i].send(data, opts);
        }
        else {
            console.log( 'Error: Client iphone-sim ('+i+') not connected.' );
        }
    }
};


// HTTP Server to accept incomming MPEG Stream
var streamServer = require('http').createServer( function(request, response) {
    var params = request.url.substr(1).split('/');
    width = (params[1] || WIDTH)|0;
    height = (params[2] || HEIGHT)|0;

    if( params[0] == STREAM_SECRET ) {
        console.log(
            'Stream Connected: ' + request.socket.remoteAddress +
            ':' + request.socket.remotePort + ' size: ' + width + 'x' + height
        );
        request.on('data', function(data){
            socketServer.broadcast(data, {binary:true});
        });
    }
    else {
        console.log(
            'Failed Stream Connection: '+ request.socket.remoteAddress +
            request.socket.remotePort + ' - wrong secret.'
        );
        response.end();
    }
}).listen(STREAM_PORT);

console.log('Listening for MPEG Stream on http://127.0.0.1:'+STREAM_PORT+'/<secret>/<width>/<height>');
console.log('Awaiting WebSocket connections on ws://127.0.0.1:'+WEBSOCKET_PORT+'/');
console.log('Awaiting WebSocket iphone-sim connections on ws://127.0.0.1:'+WEBSOCKET_SIM_PORT+'/');
