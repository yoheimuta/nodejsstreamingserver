#!bin/sh -xe
FPS="20"
VLC_PATH="/Applications/VLC.app/Contents/MacOS/VLC"
WIDTH="320"
HEIGHT="547"
TOP="63"
SERVER="127.0.0.1"

VIDEO_FIFO="/tmp/vlc-ffmpeg.raw"

for fifo in "$VIDEO_FIFO" ; do
    rm -f "$fifo"
    mkfifo "$fifo"
done

function cleanup() {
trap "" EXIT INT

    [[ ! -z "$vlc_pid" ]] && kill -9 "$vlc_pid"
    [[ ! -z "$ffmpeg_pid" ]] && kill -9 "$ffmpeg_pid"
    rm -f "$VIDEO_FIFO"
}

trap "cleanup" EXIT INT

$VLC_PATH screen:// --screen-fps="$FPS" --screen-width=$WIDTH --screen-height=$HEIGHT --screen-top=$TOP -I dummy --quiet --sout "file/dummy:$VIDEO_FIFO" &
vlc_pid=$!

# ffmpeg reads raw video from $VIDEO_FIFO, recodes it using libx264, combines it with mp3 that's been
ffmpeg -threads 0 -re \
    -f rawvideo -pix_fmt bgra -s "${WIDTH}x${HEIGHT}" -r "$FPS" -i "$VIDEO_FIFO" \
    -vcodec mpeg1video \
    -f mpeg1video -qscale 1 "http://${SERVER}:8082/mtburn/${WIDTH}/${HEIGHT}" &
ffmpeg_pid=$!

wait $ffmpeg_pid $vlc_pid
