#!/bin/sh -xe

FPS="20"
VLC_PATH="/Applications/VLC.app/Contents/MacOS/VLC"
# I don't know how this'll behave on multimon, so you might want to hard-code.
INRES='320x480'
OUTRES='320x480'

VIDEO_FIFO=/tmp/vlc-ffmpeg.raw

for fifo in "$VIDEO_FIFO" ; do
    rm -f "$fifo"
    mkfifo "$fifo"
done

# This is called when you ^C or an app quits. It kills all the processes and deletes the FIFOs.
function cleanup() {
trap "" EXIT INT

    [[ ! -z "$vlc_pid" ]] && kill -9 "$vlc_pid"
    [[ ! -z "$ffmpeg_pid" ]] && kill -9 "$ffmpeg_pid"
    rm -f "$VIDEO_FIFO"
}

trap "cleanup" EXIT INT

# VLC streams screen:// to $VIDEO_FIFO, in a raw BGRA format.
# $VLC_PATH screen:// --screen-fps=$FPS --screen-width=320 --screen-height=480 --screen-top=20 -I dummy --quiet --sout "file/dummy:$VIDEO_FIFO"
/Applications/VLC.app/Contents/MacOS/VLC screen:// --screen-fps=20 -I dummy --quiet --sout "file/dummy:/tmp/vlc-ffmpeg.raw" &
vlc_pid=$!

# ffmpeg reads raw video from $VIDEO_FIFO, recodes it using libx264, combines it with mp3 that's been
#ffmpeg -re \
#    -f rawvideo -pix_fmt bgra -s "$INRES" -r "$FPS" -i "$VIDEO_FIFO" \
#    -vcodec mpeg1video -s "$OUTRES" \
#    -f mpeg1video -qscale 1 "http://54.92.20.47:8082/mtburn/320/480" &
#ffmpeg_pid=$!

#wait $ffmpeg_pid $vlc_pid
wait $vlc_pid
